import './ExpensesList.css';
import ExpenseItem from './ExpenseItem';

const ExpensesList = (props) => {
    if(props.items.length === 0) {
        return (
            <h4 className='expenses-list__fallback'>Found no expenses.</h4>
        );
    }

    return (
        <ul className='expenses-list'>
            {props.items.map((expense) => (
                <ExpenseItem
                    key={expense.id}
                    title={expense.title}
                    date={expense.date}
                    amount={expense.amount}
                />
            ))}
        </ul>
    );
}

export default ExpensesList;